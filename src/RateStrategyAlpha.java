package src;

public class RateStrategyAlpha implements RateStrategy {

	public int computeRate(int amount) {
		return amount / 5 * 2;
	}
}
