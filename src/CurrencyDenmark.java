package src;

public class CurrencyDenmark implements CurrencyType {

	@Override
	public void insertCoin(int coinValue) throws IllegalCoinException {
		switch (coinValue) {
		case 1:
			break;
		case 2:
			break;
		case 5:
			break;
		case 10:
			break;
		case 20:
			break;
		default:
			throw new IllegalCoinException("Invalid coin: " + coinValue);
		}
	}
}
