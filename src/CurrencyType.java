package src;

public interface CurrencyType {

	/**
	 * Checks the currency being inputed to make sure it's legal in whatever the
	 * designated country is. Throws exception if it isn't.
	 * 
	 * @param coinValue
	 * @throws IllegalCoinException
	 */
	public void insertCoin(int coinValue) throws IllegalCoinException;

}
