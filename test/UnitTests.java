package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import src.CurrencyDenmark;
import src.CurrencyType;
import src.CurrencyUSA;
import src.IllegalCoinException;
import src.PayStation;
import src.PayStationImpl;
import src.RateStrategy;
import src.RateStrategyAlpha;
import src.Receipt;
import src.ReceiptImpl;

public class UnitTests {
	PayStation ps;
	CurrencyUSA dollar;
	  @Before
	  public void setUp() {
		  dollar = new CurrencyUSA();
		  RateStrategy rs = new RateStrategyAlpha();
		  ps = new PayStationImpl(rs,dollar);
	  }
	  
		/**
		 * Receipts must be able to store parking time values
		 */
		@Test
		public void shouldStoreTimeInReceipt() {
			Receipt receipt = new ReceiptImpl(30);
			assertEquals("Receipt can store 30 minute value", 30, receipt.value());
		}
		
		/**
		 * Adding Danish Currencies to American Paystation shouldn't work
		 * 
		 * @throws IllegalCoinException
		 */
		@Test(expected = IllegalCoinException.class)
		public void shouldNotExceptDanishPayment() throws IllegalCoinException {
			ps.addPayment(2);
		}
}
